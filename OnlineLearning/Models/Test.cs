﻿using System;
using System.Collections.Generic;

namespace OnlineLearning.Models
{
    public partial class Test
    {
        public Test()
        {
            TestProfiles = new HashSet<TestProfile>();
            Flashcards = new HashSet<FlashCard>();
        }

        public string Code { get; set; } = null!;
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public int CourseId { get; set; }
        public string? Password { get; set; }
        public bool? Status { get; set; }

        public virtual Course Course { get; set; } = null!;
        public virtual ICollection<TestProfile> TestProfiles { get; set; }

        public virtual ICollection<FlashCard> Flashcards { get; set; }
    }
}
