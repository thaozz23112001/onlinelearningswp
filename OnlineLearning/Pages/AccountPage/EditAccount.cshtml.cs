using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using OnlineLearning.Models;
using Microsoft.EntityFrameworkCore;

namespace OnlineLearning.Pages.AccountPage
{
    public class EditAccountModel : PageModel
    {
        private readonly SWPContext _context;

        public EditAccountModel(SWPContext context)
        {
            _context = context;
        }


        public IActionResult Edit(int id)
        {
            var account = (from p in _context.Accounts
                           where p.AccountId == id
                           select p).ToList();
            ViewData["Account"] = account;
            return Page();
        }


        [HttpPost]

        public IActionResult Edit(Account account)
        {
            _context.Accounts.Update(account);
            _context.SaveChanges();
            return RedirectToPage("ListAccount");
        }
    }
}
