using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using OnlineLearning.Models;
using Microsoft.EntityFrameworkCore;
namespace OnlineLearning.Pages.AccountPage
{
    public class ListAccountModel : PageModel
    {
        private readonly SWPContext _context;

        public List<Account> listAccount { get; set; }

        public ListAccountModel(SWPContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> OnGetAsync()
        {
            listAccount = await _context.Accounts.ToListAsync();
            return Page();
        }

    }
}
