using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using OnlineLearning.Models;

namespace OnlineLearning.Pages.SubjectPage
{
    public class ListSubjectModel : PageModel
    {
        private readonly SWPContext _context;

        [BindProperty]
        public string? Search { get; set; }
        public List<Subject> listSubject { get; set; }

        public ListSubjectModel(SWPContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> OnGetAsync(string? search)
        {
            Search = search;
            if(Search == null)
            {
                listSubject = await _context.Subjects.ToListAsync();
            }
            else
            {
                listSubject = await _context.Subjects.Where(i =>!string.IsNullOrEmpty(i.Name) && i.Name.ToLower().Contains(search.ToLower().Trim())).ToListAsync();
            }
            
            return Page();
        }

    }
}
